import Adapter from 'enzyme-adapter-react-16'
import { shallow, mount, configure } from 'enzyme'

export const fullRenderWithStore = (component, store) => {
  configure({ adapter: new Adapter() })
  const context = {
    store,
  }

  return mount(component, { context })
}

export const shallowRender = component => {
  configure({ adapter: new Adapter() })

  return shallow(component)
}
