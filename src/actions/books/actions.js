import fetch from 'cross-fetch'

const root = 'https://api.glose.com'

// ACTION TYPES
export const LOADING_BOOKS = 'LOADING_BOOKS'
export const SET_BOOKS = 'SET_BOOKS'
export const SET_BOOK = 'SET_BOOK'
export const SET_ALL_BOOKS = 'SET_ALL_BOOKS'

// ACTIONS
const loadingBooks = () => ({
  type: LOADING_BOOKS,
})

const setBooks = (books, id) => ({
  type: SET_BOOKS,
  books,
  id,
})

const setAllBooks = allBooks => ({
  type: SET_ALL_BOOKS,
  allBooks,
})

const setBook = book => ({
  type: SET_BOOK,
  book,
})

const fetchBook = id => async dispatch => {
  dispatch(loadingBooks())
  const result = await fetch(`${root}/forms/${id}`)
  const book = await result.json()
  dispatch(setBook(book))
}

export const getBook = id => (dispatch, getState) => {
  const book =
    getState().books.allbooks &&
    getState().books.allbooks.find(book => book.id === id)
  if (book) {
    dispatch(setBook(book))
  } else {
    dispatch(fetchBook(id))
  }
}

export const fetchAllBooks = ids => async dispatch => {
  const urls = ids.map(id => `${root}/forms/${id}`)
  dispatch(loadingBooks())
  const responses = await Promise.all(urls.map(url => fetch(url)))
  const json = await Promise.all(responses.map(res => res.json()))
  dispatch(setAllBooks(json))
}

const fetchBooks = id => async (dispatch, getState) => {
  dispatch(loadingBooks())
  const result = await fetch(`${root}/shelves/${id}/forms`)
  const json = await result.json()
  dispatch(setBooks(json, id))
  dispatch(fetchAllBooks(json))
}

const shouldFetchBooks = (state, id) => {
  if (state.books.id === id) {
    return false
  } else {
    return true
  }
}

export const fetchBooksIfNeeded = id => (dispatch, getState) => {
  if (shouldFetchBooks(getState(), id)) {
    return dispatch(fetchBooks(id))
  }
}
