import fetch from 'cross-fetch'

const root = 'https://api.glose.com'

// ACTION TYPES
export const LOADING_SHELVES = 'LOADING_SHELVES'
export const SET_SHELVES = 'SET_SHELVES'
export const SET_SHELVES_FAILED = 'SET_SHELVES_FAILED'

// ACTIONS
const loadingShelves = () => ({
  type: LOADING_SHELVES,
})

const setShelves = shelves => ({
  type: SET_SHELVES,
  shelves,
})

const setShelvesFailed = () => ({
  type: SET_SHELVES_FAILED,
})

const fetchBooks = () => async (dispatch, getState) => {
  dispatch(loadingShelves())
  try {
    const result = await fetch(`${root}/users/5a8411b53ed02c04187ff02a/shelves`)
    if (!result.ok) {
      throw new Error(`API Error: ${result.statusText}`)
    }
    const json = await result.json()
    dispatch(setShelves(json))
  } catch (error) {
    dispatch(setShelvesFailed(error))
  }
}

const shouldFetchShelves = state => {
  const { shelves } = state
  if (shelves && shelves.list.length > 1) {
    return false
  } else {
    return true
  }
}

export const fetchShelvesIfNeeded = () => (dispatch, getState) => {
  if (shouldFetchShelves(getState())) {
    return dispatch(fetchBooks())
  }
}
