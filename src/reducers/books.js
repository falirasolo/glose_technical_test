import {
  LOADING_BOOKS,
  SET_BOOKS,
  SET_ALL_BOOKS,
  SET_BOOK,
} from '../actions/books/actions'

// REDUCER
const reducerBooks = (
  state = { isLoading: false, id: null, list: [] },
  action = {}
) => {
  switch (action.type) {
    case LOADING_BOOKS:
      return {
        ...state,
        isLoading: true,
      }

    case SET_BOOKS:
      return {
        ...state,
        isLoading: false,
        id: action.id,
        listIds: action.books,
      }

    case SET_ALL_BOOKS:
      return {
        ...state,
        isLoading: false,
        allbooks: action.allBooks,
      }

    case SET_BOOK:
      return {
        ...state,
        book: action.book,
      }

    default:
      return {
        ...state,
      }
  }
}

export default reducerBooks
