import {
  LOADING_SHELVES,
  SET_SHELVES,
  SET_SHELVES_FAILED,
} from '../actions/shelves/actions'

// REDUCER
const reducerShelves = (
  state = { isLoading: false, list: [] },
  action = {}
) => {
  switch (action.type) {
    case LOADING_SHELVES:
      return {
        ...state,
        isLoading: true,
      }

    case SET_SHELVES:
      return {
        ...state,
        isLoading: false,
        list: action.shelves,
      }

    case SET_SHELVES_FAILED:
      return {
        ...state,
        isLoading: false,
      }

    default:
      return {
        ...state,
      }
  }
}

export default reducerShelves
