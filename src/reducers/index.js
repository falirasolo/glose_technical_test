import { combineReducers } from 'redux'

import reducerShelves from './shelves'
import reducerBooks from './books'

export default combineReducers({
  shelves: reducerShelves,
  books: reducerBooks,
})
