import React, { useEffect, useCallback } from 'react'
import humanize from 'humanize-list'
import Img from 'react-image'

import { useMappedState, useDispatch } from '../../store'
import Loader from '../../Components/Loader/Loader'
import { getBook } from '../../actions/books/actions'
import css from './Book.module.css'

const Book = ({ match, isLoading }) => {
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(getBook(match.params.id))
  }, [match.params.id])

  const { book } = useMappedState(
    useCallback(state => ({ book: state.books.book }), [])
  )

  if (!book) {
    return <Loader mode="spinner" />
  }

  function renderDescription(text) {
    return { __html: text }
  }

  const {
    image,
    title,
    description,
    isbn,
    language,
    authors,
    form,
    price,
  } = book

  return (
    <div>
      {book && (
        <div className={css.book}>
          <div className={css.left}>
            <Img
              loader={<Loader />}
              src={image}
              alt={title}
              className={css.img}
            />
            <div>
              <h3>Informations</h3>
              <div>{humanize(authors.map(author => author.name))}</div>
              <div>ISBN: {isbn}</div>
              <div>Langue: {language}</div>
              {price && (
                <div className={css.priceWrapper}>
                  <span>{form}</span>
                  <span className={css.price}>
                    {price.amount} {price.currency}
                  </span>
                </div>
              )}
            </div>
          </div>
          <div className={css.right}>
            <div>
              <h3 className={css.title}>{title}</h3>
              <p>Note moyenne: </p>
            </div>
            <div>
              <h3>Résumé</h3>
              <div dangerouslySetInnerHTML={renderDescription(description)} />
            </div>
          </div>
        </div>
      )}
    </div>
  )
}

export default Book
