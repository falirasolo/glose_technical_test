import React, { useEffect, useCallback } from 'react'

import { useMappedState, useDispatch } from '../../store'
import Loader from '../../Components/Loader/Loader'
import Book from '../../Components/Book/Book'
import { fetchBooksIfNeeded } from '../../actions/books/actions'
import css from './Books.module.css'

const Books = ({ match }) => {
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(fetchBooksIfNeeded(match.params.id))
  }, [match.params.id])

  const { isLoading, books, shelf, allbooks } = useMappedState(
    useCallback(
      state => ({
        isLoading: state.books.isLoading,
        books: state.books.listIds,
        shelf: state.shelves.list.find(shelf => shelf.id === match.params.id),
        allbooks: state.books.allbooks,
      }),
      []
    )
  )

  if (isLoading || !shelf || !books || !allbooks) {
    return <Loader mode="spinner" />
  }

  return (
    <div>
      <h2>{shelf.title}</h2>
      <div className={css.books}>
        {allbooks.map((book, index) => (
          <Book key={book.id} book={book} />
        ))}
      </div>
    </div>
  )
}

export default Books
