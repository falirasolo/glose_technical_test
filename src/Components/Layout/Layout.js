import React from 'react'

import Menu from '../Menu/Menu'
import Content from '../Content/Content'
import css from './Layout.module.css'

const Layout = ({ children }) => (
  <div className={css.wrapper}>
    <div className={css.container}>
      <Menu />
      <Content>{children}</Content>
    </div>
  </div>
)

export default Layout
