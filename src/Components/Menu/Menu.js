import React, { useEffect, useCallback } from 'react'
import { Link } from 'react-router-dom'

import { useMappedState, useDispatch } from '../../store'
import Loader from '../Loader/Loader'
import { fetchShelvesIfNeeded } from '../../actions/shelves/actions'
import css from './Menu.module.css'

const Menu = () => {
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(fetchShelvesIfNeeded())
  }, [])

  const { isLoading, shelves } = useMappedState(
    useCallback(
      state => ({
        isLoading: state.shelves.isLoading,
        shelves: state.shelves.list,
      }),
      []
    )
  )

  if (isLoading) {
    return (
      <div className={css.list}>
        <Loader mode="spinner" />
      </div>
    )
  }

  if (!isLoading && shelves.length === 0) {
    return <p>0 shelves found.</p>
  }

  return (
    <ul className={css.list}>
      <li className={css.li}>
        <Link to="/" className={css.title}>
          Etagère
        </Link>
      </li>
      {shelves &&
        shelves.map((shelf, i) => (
          <li className={css.li} key={shelf.id}>
            <Link to={`/books/${shelf.id}`} className={css.link}>
              {shelf.title}
            </Link>
          </li>
        ))}
    </ul>
  )
}

export default Menu
