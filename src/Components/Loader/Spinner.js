import React from 'react'

import css from './Spinner.module.css'

const Spinner = () => (
  <div className={css.spinner}>
    <div />
    <div />
    <div />
  </div>
)

export default Spinner
