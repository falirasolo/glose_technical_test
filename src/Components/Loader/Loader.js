import React from 'react'
import cx from 'classnames'

import Spinner from './Spinner'
import css from './Loader.module.css'

const Loader = ({ mode = 'default' }) => {
  if (mode === 'spinner') {
    return <Spinner />
  }
  return <div className={cx(css.loader, css[mode])} />
}

export default Loader
