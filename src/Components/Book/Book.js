import React from 'react'
import { Link } from 'react-router-dom'
import humanize from 'humanize-list'
import Img from 'react-image'

import Loader from '../Loader/Loader'
import css from './Book.module.css'

const Book = ({ match, id, dispatch, book }) => (
  <div className={css.book}>
    <Link to={`/book/${book.id}`} className={css.link}>
      <Img
        loader={<Loader mode="small" />}
        src={book.image}
        alt={book.title}
        className={css.img}
      />
      <div className={css.infosWrapper}>
        <p className={css.title}>{book.title}</p>
        <div className={css.infos}>
          <span className={css.rate}>{book.form}</span>
          {book.price && (
            <div className={css.price}>
              {book.price.amount} {book.price.currency}
            </div>
          )}
        </div>
        {book.authors.length > 0 && (
          <p className={css.author}>
            par {humanize(book.authors.map(author => author.name))}
          </p>
        )}
      </div>
    </Link>
  </div>
)

export default Book
