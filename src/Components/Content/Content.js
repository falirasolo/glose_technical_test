import React from 'react'

import css from './Content.module.css'

const Content = ({ children }) => <div className={css.content}>{children}</div>

export default Content
