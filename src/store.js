import { createStore, applyMiddleware, compose } from 'redux'
import thunkMiddleware from 'redux-thunk'
import { create } from 'redux-react-hook'

import reducers from './reducers'

export const { StoreContext, useDispatch, useMappedState } = create()

const composeEnhancers =
  (typeof window !== 'undefined' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) ||
  compose
composeEnhancers(applyMiddleware(thunkMiddleware))

const INITIAL_STATE = {
  shelves: { isLoading: false, list: [] },
  books: { isLoading: false, id: null, list: [] },
}

const configureStore = preloadedState =>
  createStore(
    reducers,
    INITIAL_STATE,
    composeEnhancers(applyMiddleware(thunkMiddleware))
  )

export default configureStore
