import React from 'react'
import ReactDOM from 'react-dom'
import renderer from 'react-test-renderer'

import { shallowRender } from '../testUtils'

import App from './App'

describe('App', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div')
    ReactDOM.render(<App />, div)
    ReactDOM.unmountComponentAtNode(div)
  })

  it('render App snapshot', () => {
    const wrapper = renderer.create(<App />).toJSON()
    expect(wrapper).toMatchSnapshot()

    const toto = shallowRender(<App />)
    expect(toto).toMatchSnapshot()
  })
})
