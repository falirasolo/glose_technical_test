import React from 'react'
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

import configureStore, { StoreContext } from './store'

import Main from './Main'
import Layout from './Components/Layout/Layout'
import Books from './Pages/Books/Books'
import Book from './Pages/Book/Book'
import NoMatch from './NoMatch'
import './App.css'

const store = configureStore()

console.log('store', store.getState())

const App = () => (
  <Provider store={store}>
    <StoreContext.Provider value={store}>
      <Router>
        <Layout>
          <Switch>
            <Route exact path="/" component={Main} />
            <Route path="/books/:id" component={Books} />
            <Route path="/book/:id" component={Book} />
            <Route component={NoMatch} />
          </Switch>
        </Layout>
      </Router>
    </StoreContext.Provider>
  </Provider>
)

export default App
